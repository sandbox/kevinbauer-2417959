<?php

/**
 * @file
 * Code for Flag Scheduler
 */

/**
 * Implements hook_permission().
 */
function flag_scheduler_permission() {
  return array(
    'schedule (un)flagging of nodes' => array(
      'title' => t('Schedule flagging and unflagging of nodes'),
      'description' => t('Allows users to set a start and end time for node flagging and unflagging.'),
    ),
  );
}

/**
 * Implements hook_field_extra_fields().
 */
function flag_scheduler_field_extra_fields() {

  $fields = array();

  foreach (node_type_get_types() as $type) {

    $fields['node'][$type->type]['form']['flag_scheduler'] = array(
      'label' => t('Flag Scheduler'),
      'description' => t('Fieldset containing scheduling for flags'),
      'weight' => 0,
    );
  }

  return $fields;
}

/**
 * Implements hook_form_alter().
 */
function flag_scheduler_form_alter(&$form, &$form_state, $form_id) {

  if (!empty($form['#node_edit_form']) && isset($form['uid']['#value'])) {

    $account = user_load($form['uid']['#value']);
    $has_administer_flag_access = user_access('schedule (un)flagging of nodes', $account);

    if ($has_administer_flag_access) {

      // These flags will fill the array of selectable flags for the flag sheduler.
      $flags = flag_get_flags('node', $form['#node']->type);

      // Hide the Flag Scheduler in node edit forms if there are no flags to schedule.
      // Also build the array for the checkboxes under $form['flag_scheduler']['flags_to_schedule'].

      $flags_to_schedule_checkboxes = array();

      if (!empty($flags)) {

        $access = TRUE;

        foreach ($flags as $flag) {

          $flags_to_schedule_checkboxes[$flag->name] = $flag->title;
        }
      }
      else {

        $access = FALSE;
      }

      if (!empty($form['#node']->nid)) {

        $defaults = db_query('SELECT * FROM {flag_scheduler} AS f
          WHERE f.nid = :nid', array(':nid' => $form['#node']->nid))->fetch();
      }

      $form['flag_scheduler'] = array(
        '#type' => 'fieldset',
        '#title' => t('Flag scheduling'),
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#access' => $access,
        '#group' => 'additional_settings',
      );

      $form['flag_scheduler']['flag_scheduler_description'] = array(
        '#type' => 'item',
        '#title' => t('Flag(s) to schedule'),
        '#description' => t('Select the flag(s) you wish to schedule.')
      );

      // Keys of array are used to set default values for checkboxes. If the value
      // for the key is 0, then the site editor did not select the flag for
      // scheduling.
      $default_checkboxes = array();

      if (isset($defaults->flags)) {

        $unserialized_checkboxes_array = unserialize($defaults->flags);

        foreach ($unserialized_checkboxes_array as $key => $value) {

          if ($value != 0) {

            $default_checkboxes[] = $key;
          }
        }
      }

      $form['flag_scheduler']['flag_scheduler_flags'] = array(
        '#type' => 'checkboxes',
        '#description' => '', // Create whitespace between this and the next form.
        '#required' => FALSE,
        '#options' => $flags_to_schedule_checkboxes,
        '#default_value' => isset($default_checkboxes) ? $default_checkboxes : '',
      );

      if (isset($defaults->flag_on) && $defaults->flag_on != 0) {

        $default_flag_on_date = date("Y-m-d H:i", $defaults->flag_on);
      }
      else {

        $default_flag_on_date = NULL;
      }

      $form['flag_scheduler']['flag_scheduler_flag_on'] = array(
        '#type' => 'date_popup',
        '#title' => t('Flag On'),
        '#description' => t('Set the time and date for this content to get flagged.'),
        '#required' => FALSE,
        '#default_value' => $default_flag_on_date,
      );

      if (isset($defaults->unflag_on) && $defaults->unflag_on != 0) {

        $default_unflag_on_date = date("Y-m-d H:i", $defaults->unflag_on);
      }
      else {

        $default_unflag_on_date = NULL;
      }

      $form['flag_scheduler']['flag_scheduler_unflag_on'] = array(
        '#type' => 'date_popup',
        '#title' => t('Unflag On'),
        '#description' => t('Set the time and date for this content to get unflagged.'),
        '#required' => FALSE,
        '#default_value' => $default_unflag_on_date,
      );

      array_unshift($form['#validate'], 'flag_scheduler_form_validate');
    }
  }
}

/**
 * Validation for $form['flag_scheduler']
 */
function flag_scheduler_form_validate($form, &$form_state) {

  $node = (object) $form_state['values'];

  if (!empty($node->flag_scheduler_flag_on) && !empty($node->flag_scheduler_unflag_on) && !empty($node->flag_scheduler_flags)) {

    if (strtotime($node->flag_scheduler_flag_on) >= strtotime($node->flag_scheduler_unflag_on)) {

      form_set_error('flag_scheduler', t('"Flag On" date and time must be less than the "Unflag On" date and time.'));
    }
  }

  if (!empty($node->flag_scheduler_flag_on) && !empty($node->flag_scheduler_flags)) {

    if (strtotime($node->flag_scheduler_flag_on) <= time()) {

      form_set_error('flag_scheduler_flag_on', t('"Flag On" date and time must be later than the current date and time.'));
    }
  }

  if (!empty($node->flag_scheduler_unflag_on) && !empty($node->flag_scheduler_flags)) {

    if (strtotime($node->flag_scheduler_unflag_on) <= time()) {

      form_set_error('flag_scheduler_unflag_on', t('"Unflag On" date and time must be later than the current date and time.'));
    }
  }
}

/**
 * Implements hook_node_presave().
 */
function flag_scheduler_node_presave($node) {

  // Convert the flagging times to UNIX timestamps.
  if (!empty($node->flag_scheduler_flag_on)) {

    $node->flag_scheduler_flag_on = strtotime($node->flag_scheduler_flag_on);
  }

  if (!empty($node->flag_scheduler_unflag_on)) {

    $node->flag_scheduler_unflag_on = strtotime($node->flag_scheduler_unflag_on);
  }

  // Convert the flag_scheduler_flags pseudo-field to flag IDs so we can set them
  // in {flag_scheduler}. With a checkboxes form,
  // when a checkbox is selected, the array holding the options values are set to the same
  // value as the key. For example, ['some_flag' => (Integer) 0]
  // becomes ['some_flag' => (String) 'some_flag'] when set.

  if (!empty($node->flag_scheduler_flags)) {

    foreach ($node->flag_scheduler_flags as $flag_name => $is_set_as_key) {

      if ($flag_name === $is_set_as_key) {

        // The flag was scheduled to be flagged, so retrieve the flag ID.
        $flag = flag_get_flag($flag_name);
        $node->flag_scheduler_flags[$flag_name] = intval($flag->fid);
      }
    }
  }
}

/**
 * Implements hook_node_insert().
 */
function flag_scheduler_node_insert($node) {

  if (!empty($node->flag_scheduler_flags) && (!empty($node->flag_scheduler_flag_on) ||
      !empty($node->flag_scheduler_unflag_on))) {

    if (empty($node->flag_scheduler_flag_on)) {

      $node->flag_scheduler_flag_on = 0;
    }

    if (empty($node->flag_scheduler_unflag_on)) {

      $node->flag_scheduler_unflag_on = 0;
    }

    db_insert('flag_scheduler')
        ->fields(array(
          'nid' => $node->nid,
          'flags' => serialize($node->flag_scheduler_flags),
          'flag_on' => $node->flag_scheduler_flag_on,
          'unflag_on' => $node->flag_scheduler_unflag_on,
        ))
        ->execute();
  }
}

/**
 * Implements hook_node_update().
 */
function flag_scheduler_node_update($node) {

  if (!empty($node->flag_scheduler_flags) && (!empty($node->flag_scheduler_flag_on) ||
      !empty($node->flag_scheduler_unflag_on))) {

    if (empty($node->flag_scheduler_flag_on)) {

      $node->flag_scheduler_flag_on = 0;
    }

    if (empty($node->flag_scheduler_unflag_on)) {

      $node->flag_scheduler_unflag_on = 0;
    }

    db_merge('flag_scheduler')
        ->key(array('nid' => $node->nid))
        ->fields(array(
          'nid' => intval($node->nid),
          'flags' => serialize($node->flag_scheduler_flags),
          'flag_on' => intval($node->flag_scheduler_flag_on),
          'unflag_on' => intval($node->flag_scheduler_unflag_on),
        ))
        ->execute();
  }
}

/**
 * Implements hook_node_delete().
 */
function flag_scheduler_node_delete($node) {

  db_delete('flag_scheduler')
      ->condition('nid', $node->nid)
      ->execute();
}

/**
 * Implements hook_cron().
 */
function flag_scheduler_cron() {

  _flag_scheduler_flagging_operations();
}

/**
 * Callback for flag_scheduler_cron.
 */
function _flag_scheduler_flagging_operations() {

  // Explanation for this process here: https://drupal.org/node/218104
  global $user;
  $original_user = $user;
  $old_state = drupal_save_session();
  drupal_save_session(FALSE);
  $user = user_load(1);

  // Refer to flag.inc's function flag for more information.
  _flag_scheduler_flag_nodes($user);
  _flag_scheduler_unflag_nodes($user);

  // Revert back to the original user.
  $user = $original_user;
  drupal_save_session($old_state);
}

/**
 * Called within _flag_scheduler_flagging_operations.
 */
function _flag_scheduler_flag_nodes($user) {

  $nodes_scheduled_for_flagging = db_select('flag_scheduler', 'f')
      ->fields('f')
      ->condition('f.flag_on', strtotime('now'), '<=')
      ->execute()
      ->fetchAll();

  foreach ($nodes_scheduled_for_flagging as $node) {

    $flags_to_set = unserialize($node->flags);

    foreach ($flags_to_set as $flag_name => $value) {

      if ($value != 0) {

        flag('flag', $flag_name, $node->nid, $user);
      }
    }

    // If the record does not have an unflag time, remove it. If it does have
    // an unflag time, replace the flag_on time with a 0.
    if ($node->unflag_on < 1 && $node->flag_on <= strtotime('now')) {

      db_delete('flag_scheduler')
          ->condition('nid', $node->nid)
          ->execute();
    }
    else {

      db_merge('flag_scheduler')
          ->key(array('nid' => $node->nid))
          ->fields(array(
            'nid' => $node->nid,
            'flag_on' => 0,
          ))
          ->execute();
    }
  }
}

/**
 * Called within _flag_scheduler_flagging_operations.
 */
function _flag_scheduler_unflag_nodes($user) {

  // Retrieve all ndoes that are scheduled to be unflagged.
  $nodes_scheduled_for_unflagging = db_select('flag_scheduler', 'f')
      ->fields('f')
      ->condition('f.unflag_on', strtotime('now'), '<=')
      ->condition('f.unflag_on', 0, '>')
      ->execute()
      ->fetchAll();

  foreach ($nodes_scheduled_for_unflagging as $node) {

    $flags_to_unset = unserialize($node->flags);

    foreach ($flags_to_unset as $flag_name => $value) {

      if ($value != 0) {

        flag('unflag', $flag_name, $node->nid, $user);
      }
    }

    db_delete('flag_scheduler')
        ->condition('nid', $node->nid)
        ->execute();
  }
}

/**
 * Implements hook_menu().
 */
function flag_scheduler_menu() {

  $items = array();

  $items['admin/content/flag-scheduler'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => t('Flag Scheduled'),
    'description' => t('Display a list of flag scheduled nodes'),
    'page callback' => 'flag_scheduler_list',
    'access arguments' => array(NULL),
    'file' => NULL,
  );

  return $items;
}

/**
 * Builds a table displaying all the nodes that are scheduled to be
 * (un)flagged. The list can be viewed at admin/content/flag-scheduler
 */
function flag_scheduler_list() {

  $results = db_query('SELECT * FROM {flag_scheduler} AS f
    ORDER BY f.flag_on, f.unflag_on ASC')->fetchAll();

  $rows = array();

  foreach ($results as $result) {

    // Retrieve the title and type of the node.
    $node_fields = db_query('SELECT n.title, nt.name FROM {node} AS n
      INNER JOIN {node_type} AS nt ON n.type = nt.type
      WHERE n.nid = :nid', array(':nid' => $result->nid))->fetch();

    // Generate output for the array of flags.
    $array_of_flags = unserialize($result->flags);

    $string_of_flags = '';

    foreach ($array_of_flags as $flag => $value) {

      if ($value != 0) {

        // This is just so we don't show the underscored name of the flag, but
        // instead use the 'prettier' title field of the flag.
        $flag_object = flag_get_flag($flag);
        $string_of_flags .= $flag_object->title . '<br />';
      }
    }

    // For edit and delete links.
    $destination = drupal_get_destination();
    $ops = array(
      l(t('edit'), 'node/' . $result->nid . '/edit', array('query' => $destination)),
      l(t('delete'), 'node/' . $result->nid . '/delete', array('query' => $destination)),
    );

    // Format the flag_on and unflag_on times.
    $flag_on = !empty($result->flag_on) ? date("Y-m-d H:i", $result->flag_on) : '';
    $unflag_on = !empty($result->unflag_on) ? date("Y-m-d H:i", $result->unflag_on) : '';

    $rows[] = array(
      $node_fields->title,
      $node_fields->name,
      $string_of_flags,
      $flag_on,
      $unflag_on,
      implode(' ', $ops),
    );
  }

  $header = array(t('Title'), t('Type'), t('Flags'), t('Flag On'), t('Unflag On'), t('Operations'));

  $build['flag_scheduler_list'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no scheduled flags.'),
  );

  return $build;
}
