
README
--------------------------------------------------------------------------

This module allows nodes to be flagged and unflagged on specified dates.

Dates can be entered with Date Popup in Drupal 7.

Notice:
- Please check if cron is running correctly if flag_scheduler does not flag or unflag your
  scheduled nodes.
- Flag Scheduler only schedules flagging and unflagging of nodes.
- Only users that can access node edit forms may schedule flagging and unflagging of nodes.
- All users that can access the admin and view content can view flag scheduled nodes.

This module was originally written for Drupal 7.34 by:

Kevin [dot] Bauer [at] ndsu.edu

INSTALLATION
--------------------------------------------------------------------------
1. Copy the flag_scheduler module folder to your modules directory.
2. Enable module, database schemas will be setup automatically.
3. Grant users the permission "schedule (un)flagging of nodes" so they can
   set when the nodes they create are to be (un)flagged.

The flag_scheduler will run with Drupal's cron.php, and will (un)flag nodes
timed on or before the time at which cron runs.

